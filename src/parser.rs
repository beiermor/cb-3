use crate::{C1Lexer, C1Token, ParseResult};
use crate::C1Token::*;

pub struct C1Parser<'a>{
    lexer: C1Lexer<'a>
}
const COMP_OPS: [C1Token; 6] = [Equal, NotEqual, LessEqual, GreaterEqual, Less, Greater];
const EXPR_OPS: [C1Token; 3] = [Plus, Minus, Or];
const TERM_OPS: [C1Token; 3] = [Asterisk, Slash, And];


impl C1Parser<'_>{
    pub fn parse(text: &str) -> ParseResult{
        C1Parser{lexer: C1Lexer::new(text)}.p()
    }

    fn p(&mut self) -> ParseResult{
        while self.lexer.current_token() != EOF {
            self.functiondefinition()?;
        }
        Ok(())
    }


    fn functiondefinition(&mut self) -> ParseResult{
        self.type_()?;
        self.lexer.expect(Identifier)?;
        self.lexer.expect(LeftParenthesis)?;
        self.lexer.expect(RightParenthesis)?;
        self.lexer.expect(LeftBrace)?;
        self.statementlist()?;
        self.lexer.expect(RightBrace)
    }

    fn type_(&mut self) -> ParseResult{
        let kek = [KwBoolean, KwVoid, KwFloat, KwInt];
        return if kek.contains(&self.lexer.current_token()){
            self.lexer.eat();
            return Ok(());
        }else{
            Err(format!("Unexpected token {:?} in line {} (expected type)", self.lexer.current_token(), self.lexer.current_line_number_no_options()))
        }
    }

    fn statementlist(&mut self) -> ParseResult{
        let firstsl = [LeftBrace, KwIf, KwReturn, KwPrintf, Identifier];
        while firstsl.contains(&self.lexer.current_token()) {
            self.block()?;
        }
        Ok(())
    }

    fn block(&mut self) -> ParseResult{
        if self.lexer.current_token() == LeftBrace {
            self.lexer.eat();
            self.statementlist()?;
            self.lexer.expect(RightBrace)
        }else {
            self.statement()
        }
    }

    fn statement(&mut self) -> ParseResult{
        match self.lexer.current_token() {
            KwIf => self.ifstatement(),
            KwReturn => {self.returnstatement()?; self.lexer.expect(Semicolon)},
            KwPrintf => {self.printf()?; self.lexer.expect(Semicolon)},
            Identifier => {
                if self.lexer.peek_token() == Assign {self.statassignment()?;}
                else if self.lexer.peek_token() == LeftParenthesis {self.functioncall()?;}
                else {return Err(format!("Unexpected token in line {} (expected assignment or function call)", self.lexer.current_line_number_no_options()))}
                self.lexer.expect(Semicolon)
            }
            _ => Err(format!("Unexpected token in line {} (expected statement)", self.lexer.current_line_number_no_options()))
        }
    }

    fn ifstatement(&mut self) -> ParseResult{
        self.lexer.expect(KwIf)?;
        self.lexer.expect(LeftParenthesis)?;
        self.assignment()?;
        self.lexer.expect(RightParenthesis)?;
        self.block()
    }

    fn assignment(&mut self) -> ParseResult{
        if self.lexer.peek_token() == Assign{
            self.lexer.eat();
            self.lexer.expect(Assign)?;
            self.assignment()
        } else {
            self.expr()
        }
    }

    fn expr(&mut self) -> ParseResult{
        self.simpexpr()?;
        while COMP_OPS.contains(&self.lexer.current_token()){
            self.lexer.eat();
            self.simpexpr()?;
        }
        Ok(())
    }

    fn simpexpr(&mut self) -> ParseResult{
        if self.lexer.current_token() == Minus {
            self.lexer.eat();
        }
        self.term()?;
        while EXPR_OPS.contains(&self.lexer.current_token()) {
            self.lexer.eat();
            self.term()?;
        }
        Ok(())
    }

    fn factor(&mut self) -> ParseResult{
        match self.lexer.current_token() {
            ConstInt => self.lexer.eat(),
            ConstFloat => self.lexer.eat(),
            ConstBoolean => self.lexer.eat(),
            LeftParenthesis => {
                self.lexer.eat();
                self.assignment()?;
                self.lexer.expect(RightParenthesis)?;
            }
            Identifier => {
                if self.lexer.peek_token() == LeftParenthesis {self.functioncall()?;}
                else {self.lexer.eat();}
            }
            _=>  {return Err(format!("Unexpected token {:?} in line {}", self.lexer.current_token(), self.lexer.current_line_number_no_options()))}

        }
        Ok(())
    }

    fn term(&mut self) -> ParseResult{
        self.factor()?;
        while TERM_OPS.contains(&self.lexer.current_token()){
            self.lexer.eat();
            self.factor()?;
        }
        Ok(())
    }

    fn statassignment(&mut self) -> ParseResult{
        self.lexer.expect(Identifier)?;
        self.lexer.expect(Assign)?;
        self.assignment()
    }

    fn returnstatement(&mut self) -> ParseResult{
        self.lexer.expect(KwReturn)?;
        if self.lexer.current_token() != Semicolon{
            self.assignment()?;
        }
        Ok(())
    }

    fn printf(&mut self) -> ParseResult{
        self.lexer.expect(KwPrintf)?;
        self.lexer.expect(LeftParenthesis)?;
        self.assignment()?;
        self.lexer.expect(RightParenthesis)
    }

    fn functioncall(&mut self) -> ParseResult{
        self.lexer.expect(Identifier)?;
        self.lexer.expect(LeftParenthesis)?;
        self.lexer.expect(RightParenthesis)
    }
}
